"""urlconf for the base application"""

from django.conf.urls import url
from django.conf.urls import url
from django.contrib import admin
from django.contrib.auth import views as auth_views

from .views import reset_algorithms
from .views import view_algorithms
from .views import algorithms
from .views import backend
from .views import home
from .views import live


urlpatterns = [
    url(r'^$', home, name='home'),

    url(r'^reset/$', reset_algorithms, name='reset_algorithms'),
    url(r'^algorithms/$', algorithms, name='algorithms'),
    url(r'^algorithms/(?P<slug>[\w-]+)/$', view_algorithms, name='view_algorithm'),
    url(r'^live/(?P<slug>[\w-]+)/$', live, name='live'),

    url(r'^backend/', backend, name='backend'),
    url(r'^login/$', auth_views.login, {'template_name': 'base/login.html'}, name='login'),
    url(r'^logout/$', auth_views.logout, name='logout'),
]
