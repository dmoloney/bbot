"""Views for the base app"""

from django.shortcuts import render

from apps.base.models import Algorithm
from pymongo import MongoClient


def home(request):
    """ Default view for the root """
    return render(request, 'base/home.html')


def backend(request):
    """ Default view for the root """
    context = {
        "dashboard_tab": " active open selected"
    }

    return render(request, 'base/backend.html', context)


def login(request):
    """ Default view for the root """
    return render(request, 'base/login.html')


def algorithms(request):
    algos = Algorithm.objects.filter(author_id=request.user)

    context = {
        "alogrithm_tab": " active open selected",
        "algorithms": algos
    }

    return render(request, 'base/algorithms.html', context)




def create_algo(title, text, user):
    a = Algorithm()
    a.title = title
    a.text = text
    a.author = user
    a.save()


def reset_algorithms(request):
    Algorithm.objects.all().delete()

    create_algo(title="Manchester's Winnng Run Algo", text="basic model", user=request.user)
    create_algo(title="Tutorial 101", text="basic model", user=request.user)
    create_algo(title="How to use SciKit", text="basic model", user=request.user)
    create_algo(title="How to use TensorFlow", text="basic model", user=request.user)
    create_algo(title="Betting Example", text="basic model", user=request.user)
    create_algo(title="Data Points", text="basic model", user=request.user)
    context = {
        "alogrithm_tab": " active open selected"
    }

    return render(request, 'base/algorithms.html', context)



def get_code(slug, username):

    try:
        client = MongoClient()

        db = client['CodeRepo']
        algos = db[username]
        algo_code = algos.find_one({"algo": slug})

        return algo_code['code']
    except:
        return ""


def view_algorithms(request, slug):
    algo = Algorithm.objects.get(slug=slug)

    code = get_code(slug, request.user.username)

    print("algo => " + str(algo))
    context = {
        "alogrithm_tab": " active open selected",
        "algo": algo,
        "code": code

    }

    print("slug" + slug)

    return render(request, 'base/view_algorithms.html', context)

def live(request, slug):
    algo = Algorithm.objects.get(slug=slug)

    code = get_code(slug, request.user.username)
    context = {
        "alogrithm_tab": " active open selected",
        "algo": algo,
        "code": code

    }

    print("slug" + slug)

    return render(request, 'base/live.html', context)
