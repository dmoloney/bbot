import datetime
import os
from shutil import copyfile

from pymongo import MongoClient
from slugify import slugify

from file_writer import FileWriter
from clients import get_mongo,get_rabbit

class Persistence:
    client = get_mongo()

    def save_code(self, code, user, algo):
        db = self.client['CodeRepo']

        print("user: " + str(user))
        print("algo: " + str(algo))

        algos = db[user]

        post = {"author": user,
                "algo": algo,
                "code": code,
                "date": datetime.datetime.utcnow()}

        key = {'algo': algo}
        algos.update(key, post, upsert=True)

    def write_code_to_disk(self, user, algo):
        db = self.client['CodeRepo']

        algos = db[user]

        algo_code = algos.find_one({"algo": algo})

        code = algo_code['code']
        fw = FileWriter()

        file_name = fw.write_algo_to_file(user, algo, code)

        self.copy_running_code(user, "betting.py")
        self.copy_running_code(user, "runner.py")
        self.copy_running_code(user, "football.py")
        self.copy_running_code(user, "clients.py")

        if not os.path.isfile(file_name):
            raise Exception("Could not save source code")

        return file_name

    def copy_running_code(self, user, file):
        copyfile("/Users/diarmuid/PycharmProjects/websockets/"  + file, "/tmp/code/" + slugify(user) + "/"  + file)

    def _write_to_file(self, code, file_name):
        try:
            os.remove(file_name)
        except OSError:
            pass
        f = open(file_name, 'w')
        f.write(code)
        f.close()
