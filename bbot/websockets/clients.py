import pika
from pymongo import MongoClient

IP = "10.11.31.64"


def get_rabbit():
    return pika.BlockingConnection(pika.ConnectionParameters(IP, 5672))

def get_mongo():
    return MongoClient(IP, 27017)
