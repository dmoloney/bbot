import csv

import requests
from pymongo import MongoClient
from slugify import slugify
from datetime import datetime
from clients import get_mongo,get_rabbit

AWAY_TEAM = 3
HOME_TEAM = 2
MATCH_DAY = 1
DIVISION = 0

CSV_URL = "http://www.football-data.co.uk/mmz4281/1617/E0.csv"

client = get_mongo()
db = client['FootballData']
historical_matches = db["HistoricalMatches"]
historical_matches.drop()


fobj = open("naming.txt")
lines = fobj.readlines()

naming_dictionary = {}
for line in lines:
    parts = line.split("=")
    naming_dictionary[parts[0]] = parts[1]


def find_data_point_name(d):
    if d == '':
        return

    d_ = naming_dictionary[d]
    slug = slugify(d_)

    if not slug:
        raise "Could not fund " + d

    return slug

def save_code(row):

    try:
        division_ = row[DIVISION]
        match_day_ = row[MATCH_DAY]
        home_team_ = row[HOME_TEAM]
        away_team_ = row[AWAY_TEAM]

        match_data_collection = db["HistoricalMatches"]

        key = {"football_id": division_ + "_" + match_day_ + "_" + slugify(home_team_) + "_" + slugify(away_team_)}
        data = {"key": key}

        for idx, d in enumerate(header):
            if not d:
                return

            data_point = row[idx]
            data[find_data_point_name(d)] = (data_point)


        date_object = datetime.strptime(match_day_, '%d/%m/%y')

        data['date'] = date_object

        match_data_collection.insert(data)
    except:
        print("Could not process the following row " + str(row))



for x in range(1994, 2017):
    a = str(x)[2:4]
    b = str(x+1)[2:4]
    season = a + b
    print (season)


    with requests.Session() as s:
        try:
            download = s.get("http://www.football-data.co.uk/mmz4281/"  + season + "/E0.csv")

            decoded_content = download.content.decode('utf-8')
            a.encode('utf-8').strip()

            cr = csv.reader(decoded_content.splitlines(), delimiter=',')
            my_list = list(cr)

            print(" >> " + my_list[0][0])

            header = my_list[0]
            for row in my_list:

                row_ = row[0]
                if row_ != "Div":
                    save_code(row)
        except:
            print("Could not process the following file " + str(season))



                    # db.getCollection('HistoricalMatches').find({}).sort({"date": -1})