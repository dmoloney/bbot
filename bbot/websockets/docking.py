import os

import docker

class Docking:
    client = docker.from_env()

    def stop_all_containers(self):
        list = self.client.containers()
        for l in list:
            print("Stopping " + l['Id'])
            self.client.stop(l['Id'])
            print("Stopped  " + l['Id'])

    def create_coding_docker(self):
        container = self.client.create_container(
            image='ubuntu',
            stdin_open=True,
            tty=True,
            command="/bin/echo 'Hello world'",
            volumes=['/code'],

            host_config=self.client.create_host_config(binds={
                '/tmp': {
                    'bind': '/mnt/vol2',
                    'mode': 'rw',
                },
            })
        )

        response = self.client.start(container=container.get('Id'))

        logs = self.client.logs(container)
        print(logs)
        return logs

    def _get_directory(self, file_name):
        realpath = os.path.abspath(file_name)
        dir_path = os.path.dirname(realpath)
        return dir_path

    def execute_code(self, user, name, file_name):
        mount_point = self._get_directory(file_name)
        actual_file_name = os.path.basename(file_name)
        mounted_file_name = "/code/" + actual_file_name
        remote_command = "python /code/runner.py"
        container = self.client.create_container(
            image='docker-bbot',
            stdin_open=True,
            tty=True,
            command=remote_command,
            volumes=['/code'],

            host_config=self.client.create_host_config(binds={
                mount_point: {
                    'bind': '/code',
                    'mode': 'rw',
                },
            })
        )

        response = self.client.start(container=container.get('Id'))

        logs = self.client.logs(container)
        print("repsonse " + str(logs))
        return logs
