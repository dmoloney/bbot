import os

from slugify import slugify
import shutil


class FileWriter:
    def write_algo_to_file(self, user, algo, code):
        file_name = self.define_file_name(user, algo)
        self.make_code_directory(file_name)
        print("made upper dir " + file_name)
        target = open(file_name, 'w')
        target.writelines(code)
        target.close()
        return file_name

    def make_code_directory(self, file_name):
        dir_path = self.get_directory(file_name)

        try:
            shutil.rmtree(dir_path)
        except OSError as e:
            print(str(e))
            pass

        os.makedirs(dir_path)


    def get_directory(self, file_name):
        realpath = os.path.abspath(file_name)
        dir_path = os.path.dirname(realpath)
        return dir_path

    def define_file_name(self, user, algo):
        file_name = "/tmp/code/" + slugify(user) + "/codee.py"
        return file_name
