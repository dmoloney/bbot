import pika
from clients import get_mongo,get_rabbit

connection = get_rabbit()
channel = connection.channel()
channel.queue_declare(queue='backtest', durable=True)

def callback(ch, method, properties, body):
    print(" [x] Received %r" % body)


channel.basic_consume(callback,
                      queue='backtest',
                      no_ack=True)


print(' [*] Waiting for messages. To exit press CTRL+C')
channel.start_consuming()