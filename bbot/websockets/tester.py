from pymongo import MongoClient
from clients import get_mongo,get_rabbit

client = get_mongo()
db = client['bets']

bets_db = db["bets"]

bets = bets_db.find()

for bet in bets:
    key_ = bet['match_key']
    betting_type = bet['bet_type']

    result = "home"
    print("For the match " + str(key_['football_id']) + " the outcome was " + result)

    if result == betting_type:
        print("won")

        wager = bet['wager']
        odds = bet['odds']

        winnings = wager * odds

        r = bets_db.update({'match_key': bet['match_key']
                            }, {
                               '$set': {
                                   'result': "won",

                                   'profit': winnings
                               }
                           }, upsert=False)

        print(r)
    else:
        print("lost")

        wager = bet['wager']

        winnings = wager * -1

        bets_db.update_one({'_id': bet['_id']
                            }, {
                               '$set': {
                                   'result': "lost",
                               },
                               '$set': {
                                   'profit': winnings
                               }
                           }, upsert=False)
