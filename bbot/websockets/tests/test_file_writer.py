import os
import time
import unittest
import shutil
from file_writer import FileWriter


class TestFileWriter(unittest.TestCase):
    def test_define_file_name(self):
        fw = FileWriter()

        file_name = fw.define_file_name("test-diarmuid", "my first algo")
        self.assertEqual(file_name, '/tmp/code/test-diarmuid/codee.py')

    def test_make_code_directory(self):
        fw = FileWriter()

        timer = int(round(time.time() * 1000))

        file_name = "/tmp/code/test-" + str(timer) + "/test.py"
        dir_name = "/tmp/code/test-" + str(timer)
        fw.make_code_directory(file_name)

        self.assertTrue(os.path.isdir(dir_name))

        os.rmdir(dir_name)

    def test_write_algo_to_file(self):
        fw = FileWriter()

        timer = int(round(time.time() * 1000))

        dir_name = "/tmp/code/test-" + str(timer)
        file_name = dir_name + "/write-test-random.py"


        fw.write_algo_to_file("test-" + str(timer), "write-test-random", "hello_world")



        self.assertTrue(os.path.isfile(file_name))

        file = open(file_name, 'r')

        assertable_text = file.read()
        self.assertEquals(assertable_text, "hello_world")

