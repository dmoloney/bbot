import datetime

from pymongo import MongoClient
from clients import get_mongo,get_rabbit


class Matches:
    historical_matches = None

    def __init__(self):
        client = get_mongo()
        db = client['FootballData']
        self.historical_matches = db["HistoricalMatches"]

    def historical_dates(self):
        historical_timeframe = self.historical_matches.distinct("date")

        historical_timeframe.sort()

        return historical_timeframe

    def match_stream(self, date):
        historical_matches = self.historical_matches.find({"date": date})

        list0 = []
        list45 = []
        list90 = []
        for historical_match in historical_matches:
            self.populate_match(historical_match, list0, 0)

        return list0

    def populate_match(self, historical_match, list, minutes):
        e = Event()
        e.minutes = minutes
        e.event_ts = historical_match['date']
        e.match_id = historical_match['key']
        e.match_details = historical_match
        e.home_team = historical_match['home-team']
        e.away_team = historical_match['away-team']
        list.append(e)


class Event:
    event_ts = datetime.datetime(2016, 12, 2, 11, 19, 54)
    event_id = 1
    match_id = "mufcVsEverton"
    home = 0
    away = 0
    minutes = 0
    home_team = None
    away_team = None

    match_details = None

    def odds(self, side):
        odds = self.match_details["ladbrokes-" + side + "-win-odds"]
        if not odds:
            return 1
        else:
            return odds

    def side(self, team):
        mufc = None
        oppt = None

        if (self.home_team == team):
            mufc = "home"
            oppt = "away"
        elif (self.away_team == team):
            mufc = "away"
            oppt = "home"

        return mufc, oppt
