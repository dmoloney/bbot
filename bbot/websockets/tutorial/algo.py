from tutorial.betting import place_bet


def initialize(context):
    context.message = 'hello'


def handle_data(context, data, matches):
    for match in matches:

        if (match.home_team == "Man United"):
            mufc_home_or_away, oppt = match.side("Man United")

            mufc_odds = match.odds(mufc_home_or_away)

            place_bet(1, 10, mufc_odds, data.date, match.match_details['key'], mufc_home_or_away)


