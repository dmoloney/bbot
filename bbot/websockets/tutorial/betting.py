from pymongo import MongoClient
from clients import get_mongo,get_rabbit

client = get_mongo()


def place_bet(execution, wager, odds, match_date, match_key, bet_type):
    db = client['bets']

    bets = db["bets"]

    post = {"execution": execution,
            "match_key": match_key,
            "odds": odds,
            "match_date": match_date,
            "wager": wager,
            "bet_type": bet_type,
            "result": "inprogress"}

    bets.insert(post)

    print("Executed bet")


def clear_bets():
    db = client['bets']

    bets = db["bets"]

    bets.remove()


def find_result(key_, day_matches):
    for match_day in day_matches:
        result = match_day.match_details['full-time-result']
        key = match_day.match_details['key']
        if key == key_:
            if result == "H":
                return "home"
            elif result == "D":
                return "draw"
            elif result == "A":
                return "away"


def calculate_winnings(execution, match_date):
    db = client['bets']

    bets_db = db["bets"]

    bets = bets_db.find({'match_date': match_date})

    winnings = 0
    for bet in bets:
        profit_ = float(bet['profit'])
        winnings += profit_


        print("calculating winnings for " + str(match_date) + " = " + str(winnings))

    return winnings


def close_bets(execution, match_date, day_matches):
    db = client['bets']

    bets_db = db["bets"]

    bets = bets_db.find({'match_date': match_date})

    for bet in bets:
        key_ = bet['match_key']
        betting_type = bet['bet_type']

        result = find_result(key_, day_matches)
        print("For the match " + str(key_['football_id']) + " the outcome was " + result)

        if result == betting_type:
            print("won")

            wager = float(bet['wager'])
            odds = float(bet['odds'])

            winnings = (wager * odds) - wager

            bets_db.update_one({'_id': bet['_id']
                                }, {
                                   '$set': {
                                       'result': "won",

                                       'profit': winnings
                                   }
                               }, upsert=False)
        else:
            print("lost")

            wager = bet['wager']

            winnings = wager * -1

            bets_db.update_one({'_id': bet['_id']
                                }, {
                                   '$set': {
                                       'result': "lost",

                                       'profit': winnings
                                   }
                               }, upsert=False)
