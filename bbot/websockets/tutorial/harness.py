from thebetpress.football import Matches
from tutorial.algo import initialize, handle_data
from tutorial.betting import close_bets, clear_bets, calculate_winnings


class Contex:
    def hi(self):
        pass


context = Contex()
initialize(context=context)


class Data:
    def goa(self):
        pass


clear_bets()

matches = Matches()

total_winnings = 0
for match_day in matches.historical_dates():
    data = Data()

    data.date = match_day

    match_date = data.date
    if match_date:
        # print("Simulating the following day " + str(match_date))

        day_matches = matches.match_stream(match_date)

        handle_data(context, data, day_matches)

        close_bets("1", match_date, day_matches)

        total_winnings += calculate_winnings(1, match_day)

print ("Total winnings = " + str(total_winnings))
