from flask import Flask, render_template
from flask.ext.socketio import SocketIO, emit

from algo_persistence import Persistence
from docking import Docking

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
socketio = SocketIO(app)


@app.route('/')
def index():
    return render_template('index.html')


@socketio.on('my event', namespace='/test')
def test_message(message):
    print("a")
    emit('my response', {'data': message['data']})


@socketio.on('run_back_test', namespace='/test')
def run_back_test(message):
    print("run_back_test")

    emit('code', {'data': "Back Testing Started"}, broadcast=True)

    p = Persistence()
    user = message['user_id']
    slug = message['slug']
    file_name = p.write_code_to_disk(user, slug)

    d = Docking()

    d.execute_code(user, slug, file_name)

    emit('code', {'data': "Back Testing Finished"}, broadcast=True)

    # try:
    #     p.write_code_to_disk( "Diarmuid", "My First Algo")
    #     emit('code', {'data': "Back Testing Finished"}, broadcast=True)
    #
    # except Exception as e:
    #     emit('code', {'data': "Back Testing Encountered Problems" + str(e)}, broadcast=True)


@socketio.on('save_code', namespace='/test')
def save_code(message):
    print('saving some code')

    code = message['data']


    slug = message['slug']
    user_id = message['user_id']
    p = Persistence()

    try:
        p.save_code(code, user_id, slug)
        emit('code', {'data': "Saved"}, broadcast=True)

    except:
        emit('code', {'data': "Not Saved"}, broadcast=True)


@socketio.on('connect', namespace='/test')
def test_connect():
    print('Client connect')

    emit('my response', {'data': 'Connected'})


@socketio.on('disconnect', namespace='/test')
def test_disconnect():
    print('Client disconnected')


if __name__ == '__main__':
    socketio.run(app, port=8080, debug=True)
